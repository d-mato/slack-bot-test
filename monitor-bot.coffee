SlackLibs = require '@slack/client'
RtmClient = SlackLibs.RtmClient
RTM_EVENTS = SlackLibs.RTM_EVENTS
CLIENT_EVENTS = SlackLibs.CLIENT_EVENTS

io = require 'socket.io-client'
EventEmitter = require('events').EventEmitter

class Monitor extends EventEmitter
  constructor: (server) ->
    @server = server
    status = 0 # 0: normal 1: warning
    last_pushed = Date.now()
    
    socket = io server
    socket.on 'current', (current_data) ->
      last_pushed = Date.now()
    
    setInterval =>
      # 最終応答時刻から10秒過ぎていたら異常とする
      if Date.now() - last_pushed > 10*1000
        unless status == 1
          status = 1
          @emit 'stop'
    
      else
        unless status == 0
          status = 0
          @emit 'reverse'
    , 1000

  check: ->
    new Promise (resolve) =>
      timer = setTimeout ->
        resolve 'stop'
      , 10000
      socket = io @server
      socket.on 'current', (current_data) =>
        resolve 'running'
        socket.disconnect()
        clearTimeout timer

module.exports = class MonitorBot
  constructor: (token, server, channel) ->
    @rtm = new RtmClient token, {logLevel: 'error'}
    @rtm.start()
    @defaultChannel = channel

    @rtm.on CLIENT_EVENTS.RTM.RTM_CONNECTION_OPENED, =>
      @rtm.sendMessage new Date()+'サーバー監視開始！', @defaultChannel

    @monitor = new Monitor(server)
    @monitor.on 'stop', => @alert('stop')
    @monitor.on 'running', => @alert('running')
    @monitor.on 'reverse', => @alert('reverse')

    @rtm.on RTM_EVENTS.MESSAGE, (message) =>
      console.log message
      return false if message.user == @rtm.activeUserId # 自身のメッセージの場合返信しない
      if /サーバ/.test message.text
        @rtm.sendMessage Messages.agree.randOne()+Messages.delimiter.randOne()+Messages.wait.randOne(), message.channel
        @monitor.check().then (status) =>
          @alert(status, {channel: message.channel, reply_to: message.user})

      else if /ありがと/.test message.text
        @rtm.sendMessage 'どういたしまして〜', message.channel

  alert: (status, options = {}) ->
    options.channel ||= @defaultChannel
    alert_messages =
      stop: 'サーバー止まったかも',
      running: 'サーバー動いてた'
      reverse: 'サーバー復帰したかも'

    reply = if options.reply_to then "<@"+options.reply_to+"> " else ''
    @rtm.sendMessage reply + alert_messages[status], options.channel
    
Array.prototype.randOne = -> @[Math.floor(Math.random() * @length)]

Messages =
  agree: ['了解', 'OK', 'わかった', 'はいはい〜', 'はいよ']
  delimiter: ['', '！', '、', '。', '…']
  wait: ['ちょっと待ってね', '少々お待ちください', 'しばしお待ちを']

